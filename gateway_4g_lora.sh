GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

printf "\n${GREEN}------------------------------------------------------------${NC}\n"
printf "\n${GREEN} Author: Sayan Nanta - 09/04/2022 , Davintechnology co,ltd. ${NC}\n"
printf "\n${GREEN} Script for setting Davin,AgroX Gateway Lora on Omega2+ ----${NC}\n"
printf "\n${GREEN}------------------------------------------------------------${NC}\n\n"

printf "\n${GREEN}Show Filesystem/Size${NC}\n"
df -h

printf "\n${GREEN}Update kmod...${NC}\n"
opkg update

printf "\n${GREEN}1.Installing python3(3.6.15-1).${NC}\n"
opkg install python3-dev

printf "\n${GREEN}2.Installing python3-pip(18.1-2).${NC}\n"
opkg install python3-pip

printf "\n${GREEN}3.Installing MQTT(1.6.1).${NC}\n"
pip3 install paho-mqtt

printf "\n${GREEN}4.Installing setuptools(59.6.0).${NC}\n"
pip3 install --upgrade setuptools

printf "\n${GREEN}5.Installing requests(2.27.1).${NC}\n"
pip3 install --upgrade requests

printf "\n${GREEN}6.Installing psutil(5.9.0).${NC}\n"
pip3 install psutil

printf "\n${GREEN}7.Installing OnionI2C(0.9-1).${NC}\n"
opkg install python-light pyOnionI2C
opkg install python3-onion-i2c

printf "\n${GREEN}8.Installing pyserial(3.1.1-1).${NC}\n"
opkg install python-pyserial

printf "\n${GREEN}9.Installing logging(3.6.15-1).${NC}\n"
opkg install python3-logging

printf "\n${GREEN}10.Installing digi-xbee(1.4.1).${NC}\n"
pip3 install digi-xbee

printf "\n${GREEN}11.Installing crypto(2.6.1-3).${NC}\n"
opkg install python3-crypto

printf "\n${GREEN}12.Installing Onion GPIO.${NC}\n"
opkg install adc-exp
opkg install python-light pyRelayExp
pip3 install onion-gpio-sysfs


printf "\n${GREEN}12.1 Installing Adafruit-GPIO(1.0.3).${NC}\n"
pip3 install Adafruit-GPIO

printf "\n${GREEN}Clone Firmware to Gateway.${NC}\n"
opkg install git git-http ca-bundle
git clone https://gitlab.com/sayan.nanta/gateway_omega_lora.git
ls -al ./gateway_omega_lora/

printf "\n${GREEN}Set Auto Startup /etc/init.d/agrox_boot${NC}\n"
cp agrox_boot /etc/init.d/agrox_boot
chmod +x /etc/init.d/agrox_boot
printf "Now at dir : "
pwd
ls -al /etc/init.d/
/etc/init.d/agrox_boot enable

printf "\n${GREEN}Check Auto Startup 'S99agrox_boot'${NC}\n"
ls -lh /etc/rc.d | grep agrox_boot
/etc/init.d/agrox_boot enabled && echo on
printf "\n${GREEN}Finish Gatewate setup process, Thank you...${NC}\n\n\n"

